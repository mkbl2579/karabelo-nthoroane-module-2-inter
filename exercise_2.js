/* Intermediate Phase - Module 2, Exercise 2
Predict which category will win MTN Business App of the year */

// Array of possible categories
const categories = ["Best Consumer Solution",
"Best Enterprise Solution",
"Best African Solution",
"Most Innovative Solution",
"Best Gaming Solution",
"Best Health Solution",
"Best Agricultural Solution",
"Best Educational Solution",
"Best Financial Solution",
"Best Hackathon Solution",
"Best South African Solution",
"Best Campus Cup Solution",
"Huawei AppGallery"]; 

// Randomly guess possible winner
let index = Math.floor(Math.random() * categories.length + 1);

// Display Output
msg = 'The winning category is: ' + categories[index];
console.log(msg);
