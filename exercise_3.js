/* Intermediate Phase - Module 2, Exercise 3
Reading data from Text File

Code Ref - LinuxHint : https://linuxhint.com/read-local-text-file-javascript/ */

// include system package
const fs = require("fs");

fs.readFile("campus_cup.txt", (err, data) => {
  if (err) throw err;

  console.log(data.toString());
});
