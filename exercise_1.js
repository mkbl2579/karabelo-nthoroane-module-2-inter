// Intermediate Phase - Module 2, Exercise 1

// Log User Details
let name = 'Jon Doe';
let institution = 'MTN App Academy';
let username = 'Jon_Creates';

// Generate Message
msg = 'My name is ' + name + ', a student from ' + institution + ' and my GitHub username is ' + username;

// Display Output
console.log(msg);
